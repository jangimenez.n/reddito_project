import psycopg2
from psycopg2.extras import RealDictCursor # add names of fields to cursor 
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from pydantic import BaseModel  # Schema
from typing import Optional  # Optional fields

try:
    db_connection = psycopg2.connect(
        host ='localhost',
        database= 'onetomany',
        user='postgres',
        password= 'API',
        cursor_factory= RealDictCursor
    )
    cursor = db_connection.cursor()
    print('Database connection: successful')
except Exception as error:
    print ('Database conncection failed')
    print('Error:', error)

class BlogPost(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    content: str
    writer_id: int
    published: bool = True
    rating: Optional[int] = None


##### DATA #####: Array of Blog Posts (Local list)
my_posts = [ 
    {"id": 1, "title": "Welcome to our blog", "content": "This is the beginning..."},
    {"id": 2, "title": "Top 10 best activities in Luxembourg", "content": "Our list of..."}
]

# find and return the post with the a given i
def find_post(given_id):
    for post in my_posts:
        if post["id"] == given_id:
            return post

# Find and return the index of a specific post
def find_post_index(given_id):
    for index, post in enumerate(my_posts):
        if post["id"] == given_id:
            return index

        
###### FastAPI instance name ######
app = FastAPI()  



#######################################
#### "Path Operations" or "Routes"  ###
#######################################

# GET / :
@app.get("/")  # decorator
def hello():  # function
    return {"message": "Hello from my first API"}  # response



# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/posts")
def get_posts():
    # Executing the SQL query
    cursor.execute("SELECT * FROM posts")
    # Retrive all the posts(list/array)
    database_posts = cursor.fetchall()
    return {"data": database_posts}



# POST /posts  ***CREATE NEW BLOGPOST***
@app.post("/posts", status_code=status.HTTP_201_CREATED)   # POST /posts endpoint
def create_posts(new_post: BlogPost):
    try:
        cursor.execute("INSERT INTO blogpost (title, content, writer_id) VALUES (%s, %s, %s) RETURNING *;",
                       (new_post.title, new_post.content, new_post.writer_id))
        post_dict = cursor.fetchone()
        db_connection.commit()
        return {"data": post_dict}
        
    except psycopg2.errors.ForeignKeyViolation as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail=f"Foreign key violation with writer_id")
           
         
        



#GET /posts/trending  ***GET LATEST BLOGPOST***
@app.get('/posts/trending')                                # Path order matters
def trending_posts():
    return {"data": my_posts[len(my_posts)-1]}



#GET /posts/{id_param}  ***GET POST WITH ID***
@app.get('/posts/{id_param}', status_code=status.HTTP_200_OK)                              # {id_param}  is the path parameter
def get_post(id_param: int, response: Response):
    cursor.execute("SELECT * FROM posts WHERE id = %s;",
                   (id_param,)) 
    get_id = cursor.fetchone()
    if not get_id:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
                                           
    return {"data": get_id}   



#DELETE /posts/{id_param}  ***DELETE POST WITH ID***
@app.delete('/posts/{id_param}', status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id_param: int):
    cursor.execute("DELETE FROM blogpost WHERE id = %s RETURNING*;",
                   (str(id_param))) 
    post_dict1 = cursor.fetchone()
    db_connection.commit()
    if not post_dict1:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        ) 
    return Response(status_code=status.HTTP_204_NO_CONTENT)  



#PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put('/posts/{id_param}')
def replace_post(id_param: int, updated_post: BlogPost):
    try:
        cursor.execute("UPDATE blogspot SET title=%s, content=%s, writer_id=%s  WHERE id=%s RETURNING*;",
                   (updated_post.title, updated_post.content, updated_post.writer_id, id_param, updated_post.writer_id, str(id_param)))
        corresponding_index = cursor.fetchone()
        db_connection.commit()
        if not replace_post:
            raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
        return {"data":replace_post}
    
    except psycopg2.errors.ForeignKeyViolation as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                        detail=f"Foreign key violation with writer_id")
    