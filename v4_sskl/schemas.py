from datetime import datetime
from pydantic import BaseModel, EmailStr

#UserPy
#Pydantic schema for User Body validation
class UserPy(BaseModel):
    email: EmailStr
    password: str

class User_Credentials(UserPy):
    pass

# Pydantic schema User Response  
class User_Response(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime
    class Config:             #Important for pydantic model/schema translation
        orm_mode = True
        
#BlogPost

class BlogPost(BaseModel):
    title: str
    content: str
    writer_id: int

class BlogPost_Response(BlogPost):
    id:int
    published:bool
    created_at: datetime
    writer: User_Response
    class Config:
        orm_mode = True 
    

class Token(BaseModel):
    access_token: str
    token_type:str
    

    