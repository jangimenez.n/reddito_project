from typing import List
from .database import get_db, database_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
from . import models, schemas
import sqlalchemy
from .utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from fastapi.middleware.cors import CORSMiddleware

# Create the tables if they dont exist yet
models.Base.metadata.create_all(bind=database_engine)

app = FastAPI() #Run the server

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

#Get all the Users
####
@app.get('/users', response_model=List[schemas.User_Response])
def get_users(all_users: int = Depends(jwt_manager.decode_token),db: Session = Depends(get_db)):
    all_users = db.query(models.User).all()
    return all_users

#Get all the Posts
####
@app.get('/posts', response_model=List[schemas.BlogPost_Response])
def get_posts(all_posts: int = Depends(jwt_manager.decode_token),db: Session = Depends(get_db)):
    all_posts = db.query(models.BlogPost).all()
    return all_posts


#Get my user
#####
@app.get('/users/me',response_model=schemas.User_Response)
def get_user_me(user_id: int = Depends(jwt_manager.decode_token),
                db:Session=Depends(get_db)):
    corresponding_user = db.query(models.User).filter(
        models.User.id == user_id).first()
    if not corresponding_user:
        raise HTTPException(status_code= status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found {user_id}")
    return corresponding_user


# Get users by id 
###
@app.get('/users/{uri_id}', response_model=schemas.User_Response)
def get_users(uri_id:int,corresponding_users = Depends(jwt_manager.decode_token),db: Session = Depends(get_db)):
    corresponding_users = db.query(models.User).filter(models.User.id == uri_id).first()
    if not corresponding_users:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Not corresponding user was found with id:{uri_id}")
    return corresponding_users

# Get blogs by id 
####
@app.get('/posts/{uri_id}', response_model=schemas.BlogPost_Response)
def get_posts(uri_id:int,corresponding_posts = Depends(jwt_manager.decode_token),db: Session = Depends(get_db)):
    corresponding_posts = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id).first()
    if not corresponding_posts:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Not corresponding user was found with id:{uri_id}")
    return corresponding_posts

# Creat a new User row
###
@app.post('/users',status_code=status.HTTP_201_CREATED ,response_model=schemas.User_Response)
def create_user(user_body:schemas.UserPy, db:Session=Depends(get_db)):
    #Hashing the password
    pwd_hashed = hash_manager.hash_pass(user_body.password)
    user_body.password = pwd_hashed #override the plain text with hash password    
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

#Create a new posts
###
@app.post('/posts',status_code=status.HTTP_201_CREATED ,response_model=schemas.BlogPost_Response)
def create_posts(post_body:schemas.BlogPost, db:Session=Depends(get_db),new_post = Depends(jwt_manager.decode_token),):
    new_post = models.BlogPost(**post_body.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    return new_post



#delete user
###
@app.delete('/users/{uri_id}')
def delete_user(uri_id: int,query_post = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    # find blogpost with id provided             
    query_post = db.query(models.User).filter(
        models.User.id == uri_id)
    if not query_post.first():  # Check if this post exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{uri_id}"
        )
    query_post.delete()  # DELETE the post
    db.commit()  # Save changes to the DB
    return Response(status_code=status.HTTP_204_NO_CONTENT)


#delete blogpost
####
@app.delete('/posts/{uri_id}')
def delete_post(uri_id: int,query_post = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    # find blogpost with id provided             
    query_post = db.query(models.BlogPost).filter(
        models.BlogPost.id == uri_id)
    if not query_post.first():  # Check if this post exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{uri_id}"
        )
    query_post.delete()  # DELETE the post
    db.commit()  # Save changes to the DB
    return Response(status_code=status.HTTP_204_NO_CONTENT)

# UPDATE blogpost endpoint
#
@app.put('/posts/{uri_id}', response_model=schemas.BlogPost_Response)
def update_post(uri_id: int, post_body: schemas.BlogPost,query_post = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    # find blogpost with id provided                
    query_post = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id)
    if not query_post.first():  # make sure it exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{uri_id}"
        )
    query_post.update(post_body.dict())  # Update the posts
    db.commit()  # Save changes to DB
    return query_post.first()

#Update users
###
@app.put('/users/{uri_id}', response_model=schemas.User_Response)
def update_user(uri_id: int, post_body: schemas.UserPy,query_post = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    # find blogpost with id provided                
    query_post = db.query(models.User).filter(models.User.id == uri_id)
    if not query_post.first():  # make sure it exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{uri_id}"
        )
    query_post.update(post_body.dict())  # Update the posts
    db.commit()  # Save changes to DB
    return query_post.first()       

# /Auth = endpoint
###
incorrectException = HTTPException(
            status_code= status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers= {"WWW-Authenticate": "Bearer"}
        )

# /Auth = endpoint
###
@app.post('/auth',status_code=status.HTTP_202_ACCEPTED ,response_model=schemas.Token)
def auth_user(
    user_credentials: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db)):
    #get corresponding user from databse
    corresponding_user: models.User = db.query(models.User).filter(
        models.User.email == user_credentials.username).first()
    #If not corresponding user found, raise an exception
    if not corresponding_user:
        raise incorrectException
    #Check the password
    pass_valid = hash_manager.verify_password(user_credentials.password,
                                              corresponding_user.password)
    #if password invalid, raise an exception 
    if not pass_valid:
        raise incorrectException
    jwt = jwt_manager.generate_token(corresponding_user.id)
    return jwt