from jose import jwt
from v4_sskl.utilities.jwt_manager import SERVER_KEY, ALGORITHM
import pytest


def test_create_post(create_post ,authorized_client ):
    res = authorized_client.post("/posts",
                      json={"title":"This is how we do it",
                            "content":"yeahx3",
                            "writer_id":1})    
    print(res.json())

    assert res.status_code == 201
    assert res.json().get("title") == create_post["title"]
    assert res.json().get("content") == create_post["content"]
    assert res.json().get("writer_id") == create_post["writer_id"]
    