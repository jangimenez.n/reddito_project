import pytest
from fastapi.testclient import TestClient
from v4_sskl.main import app
from v4_sskl.models import Base
from v4_sskl.database import get_db
# Create and close DB sessions
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


#DB URL
DATABASE_URL = 'postgresql://postgres:API@localhost:5432/sqlalchemy_test'

#Running engine for ORM translation (python to SQL)
database_engine = create_engine(DATABASE_URL)

# Template for the connection
TestingSessionTemplate = sessionmaker(autocommit=False,
                               autoflush=False, bind=database_engine)

# Dependency : Create and close session on-demand 
def override_get_db():
    db = TestingSessionTemplate()
    try:
        yield db
    finally:
        db.close()
        
@pytest.fixture()
def session():
    #Clean DB by deleting previos tables 
    Base.metadata.drop_all(bind=database_engine)
    #Create tables
    Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] = override_get_db

@pytest.fixture()
def client(session):
    yield  TestClient(app)
    
@pytest.fixture()
def create_user(client):
    user_credentials = {"email":"test6.user@domain.lu",
                            "password":"1234"}
    #post request to create a new user
    res = client.post("/users",
                      json = user_credentials)    
    #response json including ONLY id, email and created_at
    new_user = res.json()
    # adding password to response 
    new_user['password'] = user_credentials["password"]
    # returning new user including id, email , created_at and plain text password
    return new_user

@pytest.fixture()
def create_post(authorized_client):
    post_credentials = {"title":"This is how we do it",
                            "content":"yeahx3",
                            "writer_id":1}
    #post request to create a new user
    res = authorized_client.post("/posts",
                      json = post_credentials)    
    #response json including ONLY id, email and created_at
    new_post = res.json()
    # adding password to response 
    new_post['writer_id'] = post_credentials["writer_id"]
    # returning new user including id, email , created_at and plain text password
    return new_post



@pytest.fixture()
def user_token(create_user, client):
    res = client.post("/auth/", data = {"username": create_user['email'],
                                      "password": create_user['password']})
    return res.json().get("access_token")

@pytest.fixture()
def authorized_client(client, user_token):
    client.headers= {**client.headers, "Authorization": f"Bearer {user_token}"}
    print (client.headers["Authorization"])
    return client 


    